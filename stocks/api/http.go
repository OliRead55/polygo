package api

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"gitlab.com/OliRead55/polygo/stocks"
)

type HTTP struct {
	baseURL string
	key     string
}

func NewHTTP(baseURL string, key string) *HTTP {
	return &HTTP{
		baseURL: baseURL,
		key:     key,
	}
}

func (h *HTTP) AggregatesBars(ctx context.Context, ticker string, multiplier int, timespan stocks.Timespan, from time.Time, to time.Time, adjusted bool, sort stocks.SortOrder, limit int) (*http.Response, error) {
	endpoint := fmt.Sprintf(
		"%s/v2/aggs/ticker/%s/range/%d/%s/%s/%s",
		h.baseURL,
		ticker,
		multiplier,
		timespan,
		from.Format("2006-01-02"),
		to.Format("2006-01-02"),
	)

	query := map[string][]string{
		"adjusted": {strconv.FormatBool(adjusted)},
		"sort":     {string(sort)},
		"limit":    {strconv.Itoa(limit)},
	}

	return h.execute(ctx, endpoint, query)
}

func (h *HTTP) GroupedDailyBars(ctx context.Context, date time.Time, adjusted bool) (*http.Response, error) {
	endpoint := fmt.Sprintf(
		"%s/v2/aggs/grouped/locale/us/market/stocks/%s",
		h.baseURL,
		date.Format("2006-01-02"),
	)

	query := map[string][]string{
		"adjusted": {strconv.FormatBool(adjusted)},
	}

	return h.execute(ctx, endpoint, query)
}

func (h *HTTP) OpenClose(ctx context.Context, ticker string, date time.Time, adjusted bool) (*http.Response, error) {
	endpoint := fmt.Sprintf("%s/v1/open-close/%s/%s", h.baseURL, ticker, date.Format("2006-01-02"))

	query := map[string][]string{
		"adjusted": {strconv.FormatBool(adjusted)},
	}

	return h.execute(ctx, endpoint, query)
}

func (h *HTTP) PreviousClose(ctx context.Context, ticker string, adjusted bool) (*http.Response, error) {
	endpoint := fmt.Sprintf("%s/v2/aggs/ticker/%s/prev", h.baseURL, ticker)

	query := map[string][]string{
		"adjusted": {strconv.FormatBool(adjusted)},
	}

	return h.execute(ctx, endpoint, query)
}

func (h *HTTP) execute(ctx context.Context, endpoint string, query url.Values) (*http.Response, error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, endpoint, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Authorization", "Bearer "+h.key)

	req.URL.RawQuery = query.Encode()

	return http.DefaultClient.Do(req)
}
