package api

import (
	"context"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/OliRead55/polygo/stocks"
	"go.uber.org/zap"
)

type Logging struct {
	inner  stocks.API
	logger *zap.SugaredLogger
}

func NewLogging(logger *zap.Logger, inner stocks.API) *Logging {
	return &Logging{
		logger: logger.Sugar(),
		inner:  inner,
	}
}

func (l *Logging) AggregatesBars(ctx context.Context, ticker string, multiplier int, timespan stocks.Timespan, from time.Time, to time.Time, adjusted bool, sort stocks.SortOrder, limit int) (*http.Response, error) {
	res, err := l.inner.AggregatesBars(ctx, ticker, multiplier, timespan, from, to, adjusted, sort, limit)

	l.log(
		err,
		"ticker", ticker,
		"multiplier", multiplier,
		"timespan", timespan,
		"from", from.Format(time.RFC3339),
		"to", to.Format(time.RFC3339),
		"adjusted", adjusted,
		"sort", sort,
		"limit", limit,
	)

	return res, err
}

func (l *Logging) OpenClose(ctx context.Context, ticker string, date time.Time, adjusted bool) (*http.Response, error) {
	res, err := l.inner.OpenClose(ctx, ticker, date, adjusted)

	l.log(
		err,
		"ticker", ticker,
		"date", date.Format(time.RFC3339),
		"adjusted", strconv.FormatBool(adjusted),
		"status", res.Status,
	)

	return res, err
}

func (l *Logging) PreviousClose(ctx context.Context, ticker string, adjusted bool) (*http.Response, error) {
	res, err := l.inner.PreviousClose(ctx, ticker, adjusted)

	l.log(
		err,
		"ticker", ticker,
		"adjusted", adjusted,
	)

	return res, err
}

func (l *Logging) GroupedDailyBars(ctx context.Context, date time.Time, adjusted bool) (*http.Response, error) {
	res, err := l.inner.GroupedDailyBars(ctx, date, adjusted)

	l.log(
		err,
		"date", date.Format(time.RFC3339),
		"adjusted", adjusted,
	)

	return res, err
}

func (l *Logging) log(err error, args ...interface{}) {
	if err != nil {
		args = append(args, "err", err.Error())
		l.logger.Errorw(
			"",
			args...,
		)
	}

	l.logger.Infow(
		"",
		args...,
	)
}
