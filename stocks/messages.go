package stocks

import "time"

type AggregatesBars struct {
	Adjusted     bool                   `json:"adjusted"`
	Querycount   int                    `json:"queryCount"`
	RequestID    string                 `json:"request_id"`
	Results      []AggregatesBarsResult `json:"results"`
	ResultsCount int                    `json:"resultsCount"`
	Status       string                 `json:"status"`
	Ticker       string                 `json:"ticker"`
}

type AggregatesBarsResult struct {
	Close          float32 `json:"c"`
	High           float32 `json:"h"`
	Low            float32 `json:"l"`
	Transactions   int64   `json:"n"`
	Open           float32 `json:"o"`
	Timestamp      int64   `json:"t"`
	Volume         int64   `json:"v"`
	VolumeWeighted float32 `json:"vw"`
}

type GroupedDailyBars struct {
	Adjusted     bool                     `json:"adjusted"`
	QueryCount   int                      `json:"queryCount"`
	Results      []GroupedDailyBarsResult `json:"results"`
	ResultsCount int                      `json:"resultsCount"`
	Status       string                   `json:"status"`
}

type GroupedDailyBarsResult struct {
	Ticker       string  `json:"T"`
	Close        float32 `json:"c"`
	High         float32 `json:"h"`
	Low          float32 `json:"l"`
	Transactions int64   `json:"n"`
	Open         float32 `json:"o"`
	Timestamp    int64   `json:"t"`
	Volume       int     `json:"v"`
}

type OpenClose struct {
	AfterHours float32   `json:"afterHours"`
	Close      float32   `json:"close"`
	From       time.Time `json:"from"`
	High       float32   `json:"high"`
	Low        float32   `json:"low"`
	Open       float32   `json:"open"`
	Premarket  float32   `json:"preMarket"`
	Status     string    `json:"status"`
	Symbol     string    `json:"symbol"`
	Volume     uint      `json:"volume"`
}

type PreviousClose struct {
	Adjusted     bool                  `json:"adjusted"`
	QueryCount   int                   `json:"queryCount"`
	RequestID    string                `json:"request_id"`
	Results      []PreviousCloseResult `json:"results"`
	ResultsCount int                   `json:"resultsCount"`
	Status       string                `json:"status"`
	Ticker       string                `json:"ticker"`
}

type PreviousCloseResult struct {
	Ticker         string  `json:"T"`
	Close          float32 `json:"c"`
	High           float32 `json:"h"`
	Low            float32 `json:"l"`
	Open           float32 `json:"o"`
	Timestamp      int64   `json:"t"`
	Volume         int64   `json:"v"`
	VolumeWeighted float32 `json:"vw"`
}
