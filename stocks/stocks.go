package stocks

import (
	"context"
	"io"
	"net/http"
	"time"
)

type API interface {
	AggregatesBars(ctx context.Context, ticker string, multiplier int, timespan Timespan, from time.Time, to time.Time, adjusted bool, sort SortOrder, limit int) (*http.Response, error)
	GroupedDailyBars(ctx context.Context, date time.Time, adjusted bool) (*http.Response, error)
	OpenClose(ctx context.Context, ticker string, date time.Time, adjusted bool) (*http.Response, error)
	PreviousClose(ctx context.Context, ticker string, adjusted bool) (*http.Response, error)
}

type Parser interface {
	AggregatesBars(r io.ReadCloser) (AggregatesBars, error)
	GroupedDailyBars(reader io.ReadCloser) (GroupedDailyBars, error)
	OpenClose(reader io.ReadCloser) (OpenClose, error)
	PreviousClose(reader io.ReadCloser) (PreviousClose, error)
}

type Timespan string

const (
	TimespanMinute  = Timespan("minute")
	TimespanHour    = Timespan("hour")
	TimespanDay     = Timespan("day")
	TimespanWeek    = Timespan("week")
	TimespanMonth   = Timespan("month")
	TimespanQuarter = Timespan("quarter")
	TimespanYear    = Timespan("year")
)

type SortOrder string

const (
	SortAscending  = SortOrder("asc")
	SortDescending = SortOrder("desc")
)
