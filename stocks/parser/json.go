package parser

import (
	"encoding/json"
	"io"

	"gitlab.com/OliRead55/polygo/stocks"
)

type JSON struct{}

func (p *JSON) AggregatesBars(reader io.ReadCloser) (stocks.AggregatesBars, error) {
	defer reader.Close()

	msg := stocks.AggregatesBars{}

	return msg, json.NewDecoder(reader).Decode(&msg)
}

func (p *JSON) GroupedDailyBars(reader io.ReadCloser) (stocks.GroupedDailyBars, error) {
	defer reader.Close()

	msg := stocks.GroupedDailyBars{}

	return msg, json.NewDecoder(reader).Decode(&msg)
}

func (p *JSON) OpenClose(reader io.ReadCloser) (stocks.OpenClose, error) {
	defer reader.Close()

	msg := stocks.OpenClose{}

	return msg, json.NewDecoder(reader).Decode(&msg)
}

func (p *JSON) PreviousClose(reader io.ReadCloser) (stocks.PreviousClose, error) {
	defer reader.Close()

	msg := stocks.PreviousClose{}

	return msg, json.NewDecoder(reader).Decode(&msg)
}
