package parser_test

import (
	"io"
	"reflect"
	"strings"
	"testing"
	"time"

	"gitlab.com/OliRead55/polygo/stocks"
	"gitlab.com/OliRead55/polygo/stocks/parser"
)

func TestJSONAggregatesBarsSuccess(t *testing.T) {
	reader := io.NopCloser(strings.NewReader(`
	{
		"adjusted": true,
		"queryCount": 2,
		"request_id": "6a7e466379af0a71039d60cc78e72282",
		"results": [
		 {
		  "c": 75.0875,
		  "h": 75.15,
		  "l": 73.7975,
		  "n": 1,
		  "o": 74.06,
		  "t": 1577941200000,
		  "v": 135647456,
		  "vw": 74.6099
		 },
		 {
		  "c": 74.3575,
		  "h": 75.145,
		  "l": 74.125,
		  "n": 1,
		  "o": 74.2875,
		  "t": 1578027600000,
		  "v": 146535512,
		  "vw": 74.7026
		 }
		],
		"resultsCount": 2,
		"status": "OK",
		"ticker": "AAPL"
	}
	`))

	expect := stocks.AggregatesBars{
		Adjusted:   true,
		Querycount: 2,
		RequestID:  "6a7e466379af0a71039d60cc78e72282",
		Results: []stocks.AggregatesBarsResult{
			{
				Close:          75.0875,
				High:           75.15,
				Low:            73.7975,
				Transactions:   1,
				Open:           74.06,
				Timestamp:      1577941200000,
				Volume:         135647456,
				VolumeWeighted: 74.6099,
			},
			{
				Close:          74.3575,
				High:           75.145,
				Low:            74.125,
				Transactions:   1,
				Open:           74.2875,
				Timestamp:      1578027600000,
				Volume:         146535512,
				VolumeWeighted: 74.7026,
			},
		},
		ResultsCount: 2,
		Status:       "OK",
		Ticker:       "AAPL",
	}

	parser := parser.JSON{}
	parsed, err := parser.AggregatesBars(reader)
	if err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(expect, parsed) {
		t.Fatalf("EXPECTED: %+v\nACTUAL: %+v", expect, parsed)
	}
}

func TestJSONGroupedDailyBars(t *testing.T) {
	reader := io.NopCloser(strings.NewReader(`
	{
		"adjusted": true,
		"queryCount": 3,
		"results": [
		 {
		  "T": "C:AUDCHF",
		  "c": 0.6619247,
		  "h": 0.6643625,
		  "l": 0.6596,
		  "o": 0.6618886,
		  "t": 1599796800000,
		  "v": 160131
		 },
		 {
		  "T": "C:AUDCAD",
		  "c": 0.9598082,
		  "h": 0.9620029,
		  "l": 0.9575,
		  "o": 0.9584955,
		  "t": 1599796800000,
		  "v": 92488
		 },
		 {
		  "T": "C:AUDBGN",
		  "c": 1.2009374,
		  "h": 1.2055938,
		  "l": 1.199437,
		  "o": 1.2023558,
		  "t": 1599796800000,
		  "v": 87209
		 }
		],
		"resultsCount": 3,
		"status": "OK"
	}
	`))

	expect := stocks.GroupedDailyBars{
		Adjusted:   true,
		QueryCount: 3,
		Results: []stocks.GroupedDailyBarsResult{
			{
				Ticker:    "C:AUDCHF",
				Close:     0.6619247,
				High:      0.6643625,
				Low:       0.6596,
				Open:      0.6618886,
				Timestamp: 1599796800000,
				Volume:    160131,
			},
			{
				Ticker:    "C:AUDCAD",
				Close:     0.9598082,
				High:      0.9620029,
				Low:       0.9575,
				Open:      0.9584955,
				Timestamp: 1599796800000,
				Volume:    92488,
			},
			{
				Ticker:    "C:AUDBGN",
				Close:     1.2009374,
				High:      1.2055938,
				Low:       1.199437,
				Open:      1.2023558,
				Timestamp: 1599796800000,
				Volume:    87209,
			},
		},
		ResultsCount: 3,
		Status:       "OK",
	}

	parser := parser.JSON{}
	parsed, err := parser.GroupedDailyBars(reader)
	if err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(expect, parsed) {
		t.Fatalf("EXPECTED: %+v\nACTUAL: %+v", expect, parsed)
	}
}

func TestJSONOpenCloseSuccess(t *testing.T) {
	reader := io.NopCloser(strings.NewReader(`
	{
		"afterHours": 322.1,
		"close": 325.12,
		"from": "2020-10-14T00:00:00.000Z",
		"high": 326.2,
		"low": 322.3,
		"open": 324.66,
		"preMarket": 324.5,
		"status": "OK",
		"symbol": "AAPL",
		"volume": 26122646
	}
	`))

	expect := stocks.OpenClose{
		AfterHours: 322.1,
		Close:      325.12,
		From:       time.Date(2020, time.October, 14, 0, 0, 0, 0, time.UTC),
		High:       326.2,
		Low:        322.3,
		Open:       324.66,
		Premarket:  324.5,
		Status:     "OK",
		Symbol:     "AAPL",
		Volume:     26122646,
	}

	parser := parser.JSON{}
	parsed, err := parser.OpenClose(reader)
	if err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(expect, parsed) {
		t.Fatalf("EXPECTED: %+v\nACTUAL: %+v", expect, parsed)
	}
}

func TestJSONpreviousCloseSuccess(t *testing.T) {
	reader := io.NopCloser(strings.NewReader(`
	{
		"adjusted": true,
		"queryCount": 1,
		"request_id": "6a7e466379af0a71039d60cc78e72282",
		"results": [
		 {
		  "T": "AAPL",
		  "c": 115.97,
		  "h": 117.59,
		  "l": 114.13,
		  "o": 115.55,
		  "t": 1605042000000,
		  "v": 131704427,
		  "vw": 116.3058
		 }
		],
		"resultsCount": 1,
		"status": "OK",
		"ticker": "AAPL"
	}
	`))

	expect := stocks.PreviousClose{
		Adjusted:   true,
		QueryCount: 1,
		RequestID:  "6a7e466379af0a71039d60cc78e72282",
		Results: []stocks.PreviousCloseResult{
			{
				Ticker:         "AAPL",
				Close:          115.97,
				High:           117.59,
				Low:            114.13,
				Open:           115.55,
				Timestamp:      1605042000000,
				Volume:         131704427,
				VolumeWeighted: 116.3058,
			},
		},
		ResultsCount: 1,
		Status:       "OK",
		Ticker:       "AAPL",
	}

	parser := parser.JSON{}
	parsed, err := parser.PreviousClose(reader)
	if err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(expect, parsed) {
		t.Fatalf("EXPECTED: %+v\nACTUAL: %+v", expect, parsed)
	}
}
