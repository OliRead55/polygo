package parser

import (
	"io"

	"gitlab.com/OliRead55/polygo/stocks"
	"go.uber.org/zap"
)

type Logging struct {
	inner  stocks.Parser
	logger *zap.SugaredLogger
}

func NewLogging(logger *zap.Logger, inner stocks.Parser) *Logging {
	return &Logging{
		logger: logger.Sugar(),
		inner:  inner,
	}
}

func (l *Logging) OpenClose(r io.ReadCloser) (stocks.OpenClose, error) {
	res, err := l.inner.OpenClose(r)

	l.log(
		err,
		nil,
	)

	return res, err
}

func (l *Logging) PreviousClose(r io.ReadCloser) (stocks.PreviousClose, error) {
	res, err := l.inner.PreviousClose(r)

	l.log(
		err,
		nil,
	)

	return res, err
}

func (l *Logging) AggregatesBars(r io.ReadCloser) (stocks.AggregatesBars, error) {
	res, err := l.inner.AggregatesBars(r)

	l.log(
		err,
		nil,
	)

	return res, err
}

func (l *Logging) log(err error, args ...interface{}) {
	if err != nil {
		args = append(args, "err", err.Error())
		l.logger.Errorw(
			"",
			args...,
		)
	}

	l.logger.Infow(
		"",
		args...,
	)
}
